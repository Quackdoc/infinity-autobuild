#!/bin/bash
set -xT

# Override variables
if [ -n "$DEVELOP" ]; then
	export APP_VER="${DEV_APP_VER:-$APP_VER}"
	export IGNORE_BETA="${DEV_IGNORE_BETA:-$IGNORE_BETA}"
fi

# Install required packages
apk update
apk add --no-cache jq

# Clone upstream
[ -d "$SRCDIR/Infinity-For-Reddit" ] || git clone https://github.com/Docile-Alligator/Infinity-For-Reddit

# Check app version
export SRCDIR="$1"
export APP_VER="$(cd "$SRCDIR/Infinity-For-Reddit"; git describe --tags "$(git rev-list --tags --max-count=1)")"
export LAST_VER="$(bash -x "$SRCDIR/scripts/check_ver.sh")"

# Set app patch variables
export redirect_uri='http://127.0.0.1'
export APIUtils_file="app/src/main/java/ml/docilealligator/infinityforreddit/utils/APIUtils.java"
IS_BETA="$(echo "$APP_VER" | grep -i "beta")"
export IS_BETA

cd "$SRCDIR/Infinity-For-Reddit/" || {
	echo -e "\e[31mERROR: Failed to cd to Infinity-For-Reddit\e[0m"
	exit 1
}

# Check version
git checkout "${APP_VER}"

# Create artifact
last_ver() {
	echo "$APP_VER" >"$SRCDIR"/LAST_VER.txt
}

# Skip build if beta
if [[ -n "$IS_BETA" && "$IGNORE_BETA" == "yes" ]] && [ "$CI_PIPELINE_SOURCE" != "web" ]; then
	SKIP_BUILD=1
	echo -e "\e[32mINFO: Skip Beta version\e[0m"
fi

# Check new version
if [[ "$CI_PIPELINE_SOURCE" == "schedule" || "$CI_PIPELINE_SOURCE" == "push" ]] && [ "$LAST_VER" == "$APP_VER" ]; then
	SKIP_BUILD=1
	echo -e "\e[32mINFO: No new version\e[0m"
fi

if [ -n "$SKIP_BUILD" ]; then
	last_ver
	exit 0
fi

# Build Infinity
build_apk() {

	sed -i "s/NOe2iKrPPzwscA/$REDDIT_API/" "$APIUtils_file"
	sed -i "s|infinity://localhost|$redirect_uri|" "$APIUtils_file"
	sed -i "s|USER_AGENT = .*|USER_AGENT = \"android:personal-app:${APP_VER} (by /u/${USERNAME})\";|g" "$APIUtils_file"

	# patch gradle.properties
	sed -i "s|org.gradle.jvmargs=.*|org.gradle.jvmargs=-Xmx1920M --add-exports=jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED --add-exports=jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED --add-exports=jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED|" "$SRCDIR/Infinity-For-Reddit/gradle.properties"
	./gradlew :app:assembleMinifiedRelease
}

if build_apk; then
	echo -e "\e[32mINFO: App built successfully\e[0m"
else
	echo -e "\e[31mERROR: Failed to build apk\e[0m"
	exit 1
fi

# copy apk
APK="$SRCDIR/Infinity-For-Reddit/app/build/outputs/apk/minifiedRelease/app-minifiedRelease-unsigned.apk"
if [ -f "$APK" ]; then
	mkdir -p "$SRCDIR"/release
	cp "$APK" "$SRCDIR/release/Infinity-${APP_VER}-unsigned.apk" || {
		echo -e "\e[31mERROR: Failed to copy apk\e[0m"
		exit 1
	}

	# sign apk
	sign_apk() {
		"$SRCDIR"/scripts/apk-sign.sh "$SRCDIR/release/Infinity-${APP_VER}.apk" "$SRCDIR/release/Infinity-${APP_VER}-unsigned.apk"
	}
	if sign_apk; then
		echo -e "\e[32mINFO: Apk signed successfully\e[0m"
	else
		echo -e "\e[31mERROR: Failed to sign apk\e[0m"
		exit 1
	fi

fi

# upload apk to gitlab
APK_SIGNED="$SRCDIR/release/Infinity-${APP_VER}.apk"
UPLOAD_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Infinity/${APP_VER}/Infinity-${APP_VER}.apk"
if [ -f "$APK_SIGNED" ]; then
	upload_artifact() {
		curl -s --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$APK_SIGNED" "$UPLOAD_URL"
	}
	if upload_artifact; then
		echo -e "\n\e[32mINFO: Uploaded apk\e[0m"
		export SIGN_STATUS="OK"
	else
		echo -e "\n\e[31mERROR: Failed to upload apk\e[0m"
		exit 1
	fi
fi

# create artifact
if [ "$SIGN_STATUS" = "OK" ]; then
	last_ver
fi

# Send notifications

URL="https://api.github.com/repos/Docile-Alligator/Infinity-For-Reddit/releases/tags/$APP_VER"
CHANGELOG="$(curl -s -H "Accept: application/vnd.github+json" "$URL" | awk -F'"' '/"body":/ {print $4}')"

read -r -d '' NTFY_MESSAGE <<EOM
Infinity ${APP_VER} released.
Changelog:
$(echo -e "$CHANGELOG")
EOM

# ntfy.sh
if [ -n "$NTFY_TOPIC" ]; then
	echo -e "\e[32mINFO: Sending NTFY message\e[0m"
	"$SRCDIR/scripts/ntfy.sh" "$NTFY_MESSAGE" "$UPLOAD_URL"
fi

# Telegram
if [ -n "$TG_TOKEN" ] && [ -n "$TG_CHATID" ]; then
	echo -e "\n\e[32mINFO: Sending Telegram message\e[0m"
	"$SRCDIR/scripts/telegram.sh" "$NTFY_MESSAGE" "$APK_SIGNED"
fi
